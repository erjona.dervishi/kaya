import unittest
from kaya_index import CoIndex as ci

class TestStringMethods(unittest.TestCase):

    def test_correct_return(self):
        self.assertEquals(ci.carbon_index(2,3,4,5), 120)

    def test_raises_exception(self):
        self.assertRaises(ci.carbon_index(2,-3,4,5))

    def test_CO2_to_C(self):
        self.assertEquals(ci.carbon_index(2,3,4,5, "CO2"), ci.carbon_index(2,3,4,5, "C")/3.67)


if __name__ == '__main__':
    unittest.main()