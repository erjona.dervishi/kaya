import argparse

def setup_argparser():
    parser = argparse.ArgumentParser(prog="kaya")

    parser.add_argument("--pop", type=float, help="Population", required=True)

    parser.add_argument("--gdp", type=float, help="GDP", required=True)

    parser.add_argument("--enInt", type=float, help="Energy Intensity", required=True)

    parser.add_argument("--carbInt", type=float, help="Carbon Intensity", required=True)

    parser.add_argument("--out_typ", type=str, help="Output Type", required=False)

    return parser

def parse_args():
    parser = setup_argparser()
    args = parser.parse_args()

    pop = args.pop
    gdp = args.gdp
    enInt = args.enInt
    carbInt = args.carbInt
    out_typ = args.out_typ

    return pop, gdp, enInt, carbInt, out_typ

def carbon_index(pop, gdp, enInt, carbInt, out_typ="CO2"):
    """

    :param pop: Population size (in millions)
    :param gdp: GDP per capita (in 1000 Dollar/person)
    :param enInt: Energy Intensity needed to produce a certain amount of economic value.
    :param carbInt: Carbon Intensity is the CO2 emitted for produced energy.
    :return: Kaya identity for the yearly CO2 emissions, which is the product of all four parameters
    """

    if out_typ=="C":
        carbInt /=3.67

    args = locals()
    for k,v in args.items():
        if k == "out_typ":
            continue
        if v < 0:
            raise ValueError("Value of %s must be positive" %k)

    return pop*gdp*enInt*carbInt

if __name__ == '__main__':
    pop, gdp, enInt, carbInt, out_typ = parse_args()
    print(carbon_index(pop, gdp, enInt, carbInt, out_typ))